# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# USERS
ahmed       = User.create(email: 'ahmed@example.com',  password: 'password', name: 'Ahmed Ahmed'   )
abdul       = User.create(email: 'abdul@example.com',  password: 'password', name: 'Abdul Abdul'   )
user_three  = User.create(email: 'third@example.com',  password: 'password', name: 'Third Person'  )
user_four   = User.create(email: 'fourth@example.com', password: 'password', name: 'Fourth Person', permissions: 'administrator' )
user_five   = User.create(email: 'fifth@example.com',  password: 'password', name: 'Fifth Person',  permissions: 'impactive'     )
farhan      = User.create(email: 'farhan@example.com', password: 'password', name: 'Farhan Farhan' )

# PROJECTS
project_one   = Project.create(title: 'VSLA', description: 'VSLA')
project_two   = Project.create(title: 'Preventing SGBV in Egypt', description: 'Preventing SGBV among Syrian refugees in Egypt')

# PROJECT USERS
ProjectUser.create(project_id: project_one.id, user_id: user_three.id, roles: ['manager'])
ProjectUser.create(project_id: project_two.id, user_id: user_three.id, roles: ['manager'])
ProjectUser.create(project_id: project_one.id, user_id: ahmed.id,  roles: ['field agent'])
ProjectUser.create(project_id: project_two.id, user_id: ahmed.id,  roles: ['field agent'])
ProjectUser.create(project_id: project_one.id, user_id: abdul.id,  roles: ['field agent'])
ProjectUser.create(project_id: project_two.id, user_id: abdul.id,  roles: ['field agent'])
ProjectUser.create(project_id: project_one.id, user_id: farhan.id, roles: ['field agent'])
ProjectUser.create(project_id: project_two.id, user_id: farhan.id, roles: ['field agent'])

# GOALS
project_one_goal_one = Goal.create(project_id: project_one.id, title: 'Improved capacity for VSLA',
    description:  'New Community Based Organizations have the technical and managerial capacity '\
                  'to promote, manage, implement, and monitor the VSLA methodology in 4:6 impoverished '\
                  'communities in the targeted areas')
project_one_goal_two = Goal.create(project_id: project_one.id, title: 'Empowerment for marginalized women',
    description:  'Poor and marginalized women formed and joined the VSLA groups and received social '\
                  'empowerment and financial literacy training in targeted areas')

project_two_goal_one = Goal.create(project_id: project_two.id, title: 'Increase awareness',
    description:  'To increase awareness on SGBV and child protection among the refugee population '\
                  'living in Cairo and Alexandria.')
project_two_goal_two = Goal.create(project_id: project_two.id, title: 'Improve coping mechanisms and resilience',
    description:  'To empower refugees to develop coping mechanisms and building resilience.')
project_two_goal_three = Goal.create(project_id: project_two.id, title: 'Enhance protection',
    description:  'To enhance the protection through the availability of quality services.')

# INDICATORS
project_one_kpi_one = Indicator.create(goal_id: project_one_goal_one.id, title: 'CBOs', value_type: 'COUNT', baseline: 5, target: 50,
    description:  '# of CBOs who have adequate technical and managerial capacity to manage VSLA project '\
                  'implementation')
project_one_kpi_two = Indicator.create(goal_id: project_one_goal_one.id, title: 'VSLA groups', value_type: 'COUNT', baseline: 0, target: 12,
    description:  '# of women formed VSLA groups in the targeted areas')
project_one_kpi_three = Indicator.create(goal_id: project_one_goal_two.id, title: 'Financial literacy', value_type: 'COUNT', baseline: 0, target: 80,
    description:  '# of women received financial literacy training in the targeted areas')
project_one_kpi_four = Indicator.create(goal_id: project_one_goal_two.id, title: 'Social empowerment', value_type: 'COUNT', baseline: 0, target: 80,
    description:  '# of women received social empowerment training in the targeted areas')

project_two_kpi_one = Indicator.create(goal_id: project_two_goal_one.id, title: 'Theatre performances', value_type: 'COUNT', baseline: 45, target: 175,
    description:  'Number of individuals attending theatre performances.')
project_two_kpi_two = Indicator.create(goal_id: project_two_goal_one.id, title: 'Psycho-drama', value_type: 'COUNT', baseline: 25, target: 100,
    description:  'Number of youth attending psycho-drama ToT.')
project_two_kpi_three = Indicator.create(goal_id: project_two_goal_two.id, title: 'VSLA groups', value_type: 'COUNT', baseline: 66, target: 150,
    description:  'Number of individuals participating in VSLA groups.')
project_two_kpi_four = Indicator.create(goal_id: project_two_goal_two.id, title: 'Income generation', value_type: 'COUNT', baseline: 50, target: 200,
    description:  'Number of individuals developing an income generating activity.')

# ASSETS

asset_one           = Asset.create(user_id: ahmed.id, key: 'test_asset_one',      device_token: 'ahmed1')
asset_two           = Asset.create(user_id: ahmed.id, key: 'test_asset_two',      device_token: 'ahmed1')
asset_three         = Asset.create(user_id: ahmed.id, key: 'test_asset_three',    device_token: 'ahmed1')
asset_four          = Asset.create(user_id: ahmed.id, key: 'test_asset_four',     device_token: 'ahmed1')
asset_five          = Asset.create(user_id: ahmed.id, key: 'test_asset_five',     device_token: 'ahmed1')
asset_six           = Asset.create(user_id: ahmed.id, key: 'test_asset_six',      device_token: 'ahmed1')
asset_seven         = Asset.create(user_id: ahmed.id, key: 'test_asset_seven',    device_token: 'ahmed1')
asset_eight         = Asset.create(user_id: ahmed.id, key: 'test_asset_eight',    device_token: 'ahmed1')
asset_nine          = Asset.create(user_id: ahmed.id, key: 'test_asset_nine',     device_token: 'ahmed1')
asset_ten           = Asset.create(user_id: abdul.id, key: 'test_asset_ten',      device_token: 'abdul1')
asset_eleven        = Asset.create(user_id: abdul.id, key: 'test_asset_eleven',   device_token: 'abdul1')
asset_twelve        = Asset.create(user_id: abdul.id, key: 'test_asset_twelve',   device_token: 'abdul1')
asset_thirteen      = Asset.create(user_id: abdul.id, key: 'test_asset_thirteen', device_token: 'abdul1')
asset_fourteen      = Asset.create(user_id: abdul.id, key: 'test_asset_fourteen', device_token: 'abdul1')

# ENTRIES
draft_entry_one = Entry.create(user_id: ahmed.id, title: 'Men and Boys meeting in Gambaga', description: '40 men from 5 villages attended and reviewed curriculum and benefits of koko life. A photo of the sign in sheet is attached. Two receipts are attached.',
    created_on_manual: 1.day.ago, created_on_auto: 1.day.ago, latitude_manual: 11.1234, latitude_auto: 11.1234,
    longitude_manual: 11.1234, longitude_auto: 11.1234, status: 'draft', local_id: 'aaa', device_token: 'ahmed1')
draft_entry_two = Entry.create(user_id: ahmed.id, title: 'Visit with Koko life saleswomen in Mamprusi', description: "Reminded of main sales talking points. Was told there's a major challenge with communicating health benefit",
    created_on_manual: 2.days.ago, created_on_auto: 2.days.ago, latitude_manual: 11.1234, latitude_auto: 11.1234,
    longitude_manual: 11.1234, longitude_auto: 11.1234, status: 'draft', local_id: 'aab', device_token: 'ahmed1')
draft_entry_three = Entry.create(user_id: abdul.id, title: 'VSLA group meeting in West Village', description: 'Total of 59 participants. 1 registered participant did not attend.',
    created_on_manual: 4.days.ago, created_on_auto: 4.days.ago, latitude_manual: 11.1234, latitude_auto: 11.1234,
    longitude_manual: 11.1234, longitude_auto: 11.1234, status: 'draft', local_id: 'aac', device_token: 'abdul1')

submitted_entry_one = Entry.create(user_id: ahmed.id, title: 'Womens meeting in Gambaga', description: '30 women attended and reviewed curriculum and benefits of koko life',
    created_on_manual: 7.days.ago, created_on_auto: 7.days.ago, latitude_manual: 11.1234, latitude_auto: 11.1234,
    longitude_manual: 11.1234, longitude_auto: 11.1234, status: 'submitted', local_id: 'aae', device_token: 'ahmed1')
submitted_entry_two = Entry.create!(user_id: ahmed.id, title: 'Koko life info session', description: '',
    created_on_manual: 10.days.ago, created_on_auto: 10.days.ago, latitude_manual: 11.1234, latitude_auto: 11.1234,
    longitude_manual: 11.1234, longitude_auto: 11.1234, status: 'submitted', local_id: 'aaf', device_token: 'ahmed1')
submitted_entry_three = Entry.create(user_id: abdul.id, title: 'Meeting with elected official', description: 'Went well',
    created_on_manual: 12.days.ago, created_on_auto: 12.days.ago, latitude_manual: 11.1234, latitude_auto: 11.1234,
    longitude_manual: 11.1234, longitude_auto: 11.1234, status: 'submitted', local_id: 'aag', device_token: 'abdul1')

published_entry_one = Entry.create(user_id: ahmed.id, title: 'Published Entry One', description: 'Published Entry One',
    created_on_manual: 1.month.ago, created_on_auto: 1.month.ago, latitude_manual: 11.1234, latitude_auto: 11.1234,
    longitude_manual: 11.1234, longitude_auto: 11.1234, status: 'published', local_id: 'aai', device_token: 'ahmed1')
published_entry_two = Entry.create(user_id: ahmed.id, title: 'Published Entry Two', description: 'Published Entry Two',
    created_on_manual: 1.month.ago, created_on_auto: 1.month.ago, latitude_manual: 11.1234, latitude_auto: 11.1234,
    longitude_manual: 11.1234, longitude_auto: 11.1234, status: 'published', local_id: 'aaj', device_token: 'ahmed1')
published_entry_three = Entry.create(user_id: ahmed.id, title: 'Published Entry Three', description: 'Published Entry Three',
    created_on_manual: 1.month.ago, created_on_auto: 1.month.ago, latitude_manual: 11.1234, latitude_auto: 11.1234,
    longitude_manual: 11.1234, longitude_auto: 11.1234, status: 'published', local_id: 'aak', device_token: 'ahmed1')
published_entry_four = Entry.create(user_id: abdul.id, title: 'Published Entry Four', description: 'Published Entry Four',
    created_on_manual: 1.month.ago, created_on_auto: 1.month.ago, latitude_manual: 11.1234, latitude_auto: 11.1234,
    longitude_manual: 11.1234, longitude_auto: 11.1234, status: 'published', local_id: 'aal', device_token: 'abdul1')

# EFFECTS

Effect.create(entry_id: draft_entry_three.id, indicator_id: project_two_kpi_two.id, value_one: 0)

Effect.create(entry_id: submitted_entry_one.id, indicator_id: project_one_kpi_one.id, value_one: 0)
Effect.create(entry_id: submitted_entry_one.id, indicator_id: project_one_kpi_two.id, value_one: 0)
Effect.create(entry_id: submitted_entry_one.id, indicator_id: project_one_kpi_three.id, value_one: 0)
Effect.create(entry_id: submitted_entry_two.id, indicator_id: project_two_kpi_two.id, value_one: 0)
Effect.create(entry_id: submitted_entry_three.id, indicator_id: project_one_kpi_four.id, value_one: 0)

Effect.create(entry_id: published_entry_one.id, indicator_id: project_one_kpi_four.id, value_one: 2)
Effect.create(entry_id: published_entry_one.id, indicator_id: project_one_kpi_two.id, value_one: 3)
Effect.create(entry_id: published_entry_two.id, indicator_id: project_two_kpi_three.id, value_one: 1)
Effect.create(entry_id: published_entry_two.id, indicator_id: project_two_kpi_two.id, value_one: 4)
Effect.create(entry_id: published_entry_three.id, indicator_id: project_two_kpi_four.id, value_one: 5)
Effect.create(entry_id: published_entry_four.id, indicator_id: project_one_kpi_one.id, value_one: 4)

# ATTACHMENTS
attachment_one          = Attachment.create(entry_id: submitted_entry_one.id, asset_id: asset_one.id)
attachment_two          = Attachment.create(entry_id: submitted_entry_one.id, asset_id: asset_two.id)
attachment_three        = Attachment.create(entry_id: submitted_entry_one.id, asset_id: asset_three.id)
submitted_entry_one.update(featured_image_asset_key: attachment_one.asset.key)
attachment_four         = Attachment.create(entry_id: submitted_entry_two.id, asset_id: asset_four.id)
attachment_five         = Attachment.create(entry_id: submitted_entry_two.id, asset_id: asset_five.id)
submitted_entry_two.update(featured_image_asset_key: attachment_four.asset.key)
attachment_six          = Attachment.create(entry_id: submitted_entry_three.id, asset_id: asset_ten.id)
attachment_seven        = Attachment.create(entry_id: submitted_entry_three.id, asset_id: asset_eleven.id)
attachment_eight        = Attachment.create(entry_id: submitted_entry_three.id, asset_id: asset_twelve.id)
submitted_entry_three.update(featured_image_asset_key: attachment_six.asset.key)
attachment_eleven       = Attachment.create(entry_id: published_entry_one.id, asset_id: asset_six.id)
attachment_twelve       = Attachment.create(entry_id: published_entry_one.id, asset_id: asset_seven.id)
attachment_thirteen     = Attachment.create(entry_id: published_entry_one.id, asset_id: asset_eight.id)
published_entry_one.update(featured_image_asset_key: attachment_eleven.asset.key)
attachment_fourteen     = Attachment.create(entry_id: published_entry_two.id, asset_id: asset_nine.id)
attachment_fifteen      = Attachment.create(entry_id: published_entry_two.id, asset_id: asset_one.id)
published_entry_two.update(featured_image_asset_key: attachment_fourteen.asset.key)
attachment_sixteen      = Attachment.create(entry_id: published_entry_three.id, asset_id: asset_four.id)
attachment_seventeen    = Attachment.create(entry_id: published_entry_three.id, asset_id: asset_three.id)
published_entry_three.update(featured_image_asset_key: attachment_sixteen.asset.key)
attachment_eightteen    = Attachment.create(entry_id: published_entry_four.id, asset_id: asset_eleven.id)
attachment_nineteen     = Attachment.create(entry_id: published_entry_four.id, asset_id: asset_twelve.id)
attachment_twenty       = Attachment.create(entry_id: published_entry_four.id, asset_id: asset_fourteen.id)
published_entry_four.update(featured_image_asset_key: attachment_eightteen.asset.key)
