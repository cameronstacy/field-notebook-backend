class AddStatusToEffects < ActiveRecord::Migration[5.0]
  def change
    add_column :effects, :status, :string, default: 'active'
  end
end
