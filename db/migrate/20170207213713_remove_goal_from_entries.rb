class RemoveGoalFromEntries < ActiveRecord::Migration[5.0]
  def change
    remove_column :entries, :goal_id
  end
end
