class CreateEffects < ActiveRecord::Migration[5.0]
  def change
    create_table :effects do |t|
      t.references :indicator, foreign_key: true
      t.references :entry, foreign_key: true
      t.string :values, array: true, default: '{}'

      t.timestamps
    end
  end
end
