class UpdateColumnNamesToMatchClientApp < ActiveRecord::Migration[5.0]
  def change
    rename_column :entries, :latitude, :latitude_manual
    rename_column :entries, :longitude, :longitude_manual
    rename_column :entries, :user_created_at, :created_on_manual
    rename_column :entries, :user_created_at_auto, :created_on_auto
    rename_column :entries, :state, :status
  end
end
