class AddBaselineAndTargetToIndicators < ActiveRecord::Migration[5.0]
  def change
    add_column :indicators, :baseline, :integer
    add_column :indicators, :target, :integer
  end
end
