class CreateAssets < ActiveRecord::Migration[5.0]
  def change
    create_table :assets do |t|
      t.references :entry, foreign_key: true
      t.string :file_name

      t.timestamps
    end
  end
end
