class UpdateEffectColumns < ActiveRecord::Migration[5.0]
  def change
    remove_column :effects, :values
    remove_column :indicators, :value_names
    
    add_column :effects, :value_one, :integer
    add_column :effects, :value_two, :integer
  end
end
