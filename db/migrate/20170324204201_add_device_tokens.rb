class AddDeviceTokens < ActiveRecord::Migration[5.0]
  def change
    add_column :entries, :device_token, :string
    add_column :assets, :device_token, :string
    remove_index :entries, name: :index_entries_on_user_id_and_local_id
    remove_index :assets, name: :index_assets_on_user_id_and_key
    add_index :entries, [:user_id, :local_id, :device_token], unique: true
    add_index :assets, [:user_id, :key, :device_token], unique: true
  end
end
