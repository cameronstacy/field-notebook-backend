class CreateIndicators < ActiveRecord::Migration[5.0]
  def change
    create_table :indicators do |t|
      t.references :goal, foreign_key: true
      t.string :title
      t.string :description
      t.string :value_type
      t.string :value_names, array: true, default: '{}'

      t.timestamps
    end
  end
end
