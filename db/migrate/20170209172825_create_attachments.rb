class CreateAttachments < ActiveRecord::Migration[5.0]
  def change
    create_table :attachments do |t|
      t.references :asset, foreign_key: true
      t.references :entry, foreign_key: true
      t.datetime :start_date
      t.datetime :stop_date

      t.timestamps
    end
  end
end
