class AddDeviceIdToEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :entries, :device_id, :string
  end
end
