class CreateSnapshots < ActiveRecord::Migration[5.0]
  def change
    create_table :snapshots do |t|
      t.string :public_id
      t.references :project, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :snapshots, :public_id, unique: true
  end
end
