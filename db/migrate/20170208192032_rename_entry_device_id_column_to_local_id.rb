class RenameEntryDeviceIdColumnToLocalId < ActiveRecord::Migration[5.0]
  def change
    rename_column :entries, :device_id, :local_id
    add_index :entries, [:user_id, :local_id], unique: true
  end
end
