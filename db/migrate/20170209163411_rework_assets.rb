class ReworkAssets < ActiveRecord::Migration[5.0]
  def change
    remove_column :assets, :entry_id
    add_column :assets, :user_id, :integer
    rename_column :assets, :file_name, :local_id
    add_index :assets, [:user_id, :local_id], unique: true
  end
end
