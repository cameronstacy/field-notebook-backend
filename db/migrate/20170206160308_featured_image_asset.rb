class FeaturedImageAsset < ActiveRecord::Migration[5.0]
  def change
    add_column :entries, :featured_image_asset_id, :integer
  end
end
