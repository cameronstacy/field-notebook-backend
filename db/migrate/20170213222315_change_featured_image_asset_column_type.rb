class ChangeFeaturedImageAssetColumnType < ActiveRecord::Migration[5.0]
  def change
    change_column :entries, :featured_image_asset_id, :string
  end
end
