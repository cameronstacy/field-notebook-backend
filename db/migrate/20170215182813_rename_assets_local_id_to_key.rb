class RenameAssetsLocalIdToKey < ActiveRecord::Migration[5.0]
  def change
    rename_column :assets, :local_id, :key
    rename_column :entries, :featured_image_asset_id, :featured_image_asset_key
  end
end
