class CreateEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :entries do |t|
      t.references  :goal,                  foreign_key: true
      t.references  :user,                  foreign_key: true
      t.string      :title
      t.string      :description
      t.string      :state
      t.datetime    :user_created_at
      t.datetime    :user_created_at_auto
      t.decimal     :latitude,              {:precision=>10, :scale=>6}
      t.decimal     :latitude_auto,         {:precision=>10, :scale=>6}
      t.decimal     :longitude,             {:precision=>10, :scale=>6}
      t.decimal     :longitude_auto,        {:precision=>10, :scale=>6}

      t.timestamps
    end
  end
end
