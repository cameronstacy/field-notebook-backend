# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171225211245) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assets", force: :cascade do |t|
    t.string   "key"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "user_id"
    t.string   "device_token"
    t.index ["user_id", "key", "device_token"], name: "index_assets_on_user_id_and_key_and_device_token", unique: true, using: :btree
  end

  create_table "attachments", force: :cascade do |t|
    t.integer  "asset_id"
    t.integer  "entry_id"
    t.datetime "start_date"
    t.datetime "stop_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["asset_id"], name: "index_attachments_on_asset_id", using: :btree
    t.index ["entry_id"], name: "index_attachments_on_entry_id", using: :btree
  end

  create_table "effects", force: :cascade do |t|
    t.integer  "indicator_id"
    t.integer  "entry_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "value_one"
    t.integer  "value_two"
    t.string   "status",       default: "active"
    t.index ["entry_id"], name: "index_effects_on_entry_id", using: :btree
    t.index ["indicator_id"], name: "index_effects_on_indicator_id", using: :btree
  end

  create_table "entries", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "description"
    t.string   "status"
    t.datetime "created_on_manual"
    t.datetime "created_on_auto"
    t.decimal  "latitude_manual",          precision: 10, scale: 6
    t.decimal  "latitude_auto",            precision: 10, scale: 6
    t.decimal  "longitude_manual",         precision: 10, scale: 6
    t.decimal  "longitude_auto",           precision: 10, scale: 6
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "local_id"
    t.string   "featured_image_asset_key"
    t.string   "device_token"
    t.index ["user_id", "local_id", "device_token"], name: "index_entries_on_user_id_and_local_id_and_device_token", unique: true, using: :btree
    t.index ["user_id"], name: "index_entries_on_user_id", using: :btree
  end

  create_table "goals", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "project_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["project_id"], name: "index_goals_on_project_id", using: :btree
  end

  create_table "indicators", force: :cascade do |t|
    t.integer  "goal_id"
    t.string   "title"
    t.string   "description"
    t.string   "value_type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "baseline"
    t.integer  "target"
    t.index ["goal_id"], name: "index_indicators_on_goal_id", using: :btree
  end

  create_table "project_users", force: :cascade do |t|
    t.integer  "project_id"
    t.integer  "user_id"
    t.string   "roles",      default: [],              array: true
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["project_id"], name: "index_project_users_on_project_id", using: :btree
    t.index ["user_id"], name: "index_project_users_on_user_id", using: :btree
  end

  create_table "projects", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "hero_file_name"
    t.string   "hero_content_type"
    t.integer  "hero_file_size"
    t.datetime "hero_updated_at"
  end

  create_table "snapshots", force: :cascade do |t|
    t.string   "public_id"
    t.integer  "project_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_snapshots_on_project_id", using: :btree
    t.index ["public_id"], name: "index_snapshots_on_public_id", unique: true, using: :btree
    t.index ["user_id"], name: "index_snapshots_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "nickname"
    t.string   "image"
    t.string   "email"
    t.json     "tokens"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "permissions"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree
  end

  add_foreign_key "attachments", "assets"
  add_foreign_key "attachments", "entries"
  add_foreign_key "effects", "entries"
  add_foreign_key "effects", "indicators"
  add_foreign_key "entries", "users"
  add_foreign_key "goals", "projects"
  add_foreign_key "indicators", "goals"
  add_foreign_key "project_users", "projects"
  add_foreign_key "project_users", "users"
  add_foreign_key "snapshots", "projects"
  add_foreign_key "snapshots", "users"
end
