Rails.application.routes.draw do

  namespace :admin do
    resources :users
    resources :projects
    resources :goals
    resources :indicators
    resources :project_users

    root to: "users#index"
  end

  devise_for :users

  devise_scope :user do
      authenticated :user do
          resources :projects, only: [:index, :show]
          resources :snapshots, only: [:create]
          post 'snapshots/share', to: 'snapshots#share'
          patch 'entries/:id', to: 'entries#update'
          get 'entries/:id', to: 'entries#show'
          get 'effects', to: 'effects#create'
          get 'projects/:id/:status(/indicator/:indicator_id)', to: 'projects#show', as: 'filtered_project', defaults: { status: 'shared' }
          root "projects#index"
      end

      unauthenticated do
        get 'share/:id(/indicator/:indicator_id)', to: 'snapshots#show', as: 'report'
        get 'share/:public_id/entry/:id', to: 'entries#show_in_report', as: 'report_entry'
        root to: "devise/sessions#new"
      end
  end


  namespace :auth do
      mount_devise_token_auth_for 'User', skip: [:omniauth_callbacks], controllers: { sessions: 'overrides/sessions', passwords: 'overrides/passwords' }
  end

  api_version(:module => "V20170220", :header => {:name => "Accept", :value => "v20170220"}) do
    resources :entries, only: [:create, :index, :show, :update]
    resources :projects, only: [:create, :index]
  end

  root to: 'devise/sessions#new'
end
