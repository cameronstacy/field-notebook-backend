# Paperclip is only used for images being uploaded through admin UI, since
# these are small and not customer-facing. Images from devices in the field
# are uploaded directly to S3, and the API handles generating pre-signed URLs
# as appropriate.

Paperclip::Attachment.default_options[:storage] = :s3
Paperclip::Attachment.default_options[:s3_credentials] = {
  bucket: 		   ENV.fetch('AWS_UPLOAD_BUCKET'),
  access_key_id: 	   ENV.fetch('AWS_ACCESS_KEY_ID'),
  secret_access_key: ENV.fetch('AWS_SECRET_ACCESS_KEY'),
  s3_region: 		   ENV.fetch('AWS_REGION'),
}
