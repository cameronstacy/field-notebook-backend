namespace :daily_emails do
    desc "Kick off daily emails"
    task :send => :environment do
        Project.send_all_queued_entries_emails
        User.send_all_queued_entries_emails
    end
end
