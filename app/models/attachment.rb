class Attachment < ApplicationRecord
  belongs_to :asset
  belongs_to :entry

  def s3_object
    @obj ||= Aws::S3::Resource::new.bucket(ENV['AWS_UPLOAD_BUCKET']).object(asset.s3_path)
  end

  def download_url(secs = 1.week.to_i)
    begin
        s3_object.exists?
        s3_object.presigned_url(:get, expires_in: secs)
    rescue
        return nil
    end
  end
end
