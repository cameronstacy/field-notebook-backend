class Asset < ApplicationRecord
  validates_presence_of :device_token
  validates_presence_of :key
  validates_uniqueness_of :key, :scope => [:user_id, :device_token]

  belongs_to :user
  has_many :attachments
  has_many :entries, through: :attachments

  def s3_path
    if self.device_token then
        return "assets/#{self.user_id}/#{self.device_token}/#{self.key}"
    else
        return "assets/#{self.user_id}/#{self.key}"
    end
  end
end
