class Indicator < ApplicationRecord
  validates_presence_of :title
  validates_presence_of :description
  validates_presence_of :value_type

  belongs_to :goal
  has_one :project, through: :goal
  has_many :effects
  has_many :entries, through: :effects

  def self.traffic_light_definitions
    return [ [ 'Off track',  0   ],
             [ 'Needs work', 50  ],
             [ 'On track',   100 ] ]
  end

  def relevant_effects(point_in_time)
    effects = self.effects.joins(:entry)
                    .where(status: 'active')
                    .merge(Entry.published)
    effects = effects.merge(Entry.created_before(point_in_time)) unless point_in_time.nil?
    effects
  end

  def value_one_sum(point_in_time)
    relevant_effects(point_in_time).sum(:value_one)
  end

  def value_two_sum(point_in_time)
    relevant_effects(point_in_time).sum(:value_two)
  end

  def cumulative_value_one
    is_traffic_light? ? last_value(Time.now) : value_one_sum(Time.now)
  end

  def cumulative_value_two
    is_traffic_light? ? false : value_two_sum(Time.now)
  end

  def last_value(point_in_time)
    relevant_effects(point_in_time).last.value_one rescue 0
  end

  def as_count(point_in_time)
    value_one_sum(point_in_time) + baseline rescue 0
  end

  def as_percent(point_in_time)
    return baseline if value_two_sum(point_in_time) == 0
    (value_one_sum(point_in_time).to_f + baseline) / (value_two_sum(point_in_time).to_f + 100.0)
  end

  def as_traffic_light(point_in_time)
    return :red    if last_value(point_in_time) == 0
    return :yellow if last_value(point_in_time) == 50
    return :green  if last_value(point_in_time) == 100
    return :yellow if last_value(point_in_time).nil?
  end

  def current_value(point_in_time)
    return as_count(point_in_time)         if is_count?
    return as_percent(point_in_time)       if is_percent?
    return as_traffic_light(point_in_time) if is_traffic_light?
  end

  def target_percent_achieved(point_in_time)
    return as_percent(point_in_time)                    if is_percent?
    return (as_count(point_in_time).to_f / target.to_f) if is_count?
    return false                                        if is_traffic_light?
  end

  def is_count?
    self.value_type.eql?('COUNT')
  end

  def is_percent?
    self.value_type.eql?('PERCENT')
  end

  def is_traffic_light?
    self.value_type.eql?('TRAFFIC_LIGHT')
  end
end
