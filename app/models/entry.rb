class Entry < ApplicationRecord
  validates_presence_of :device_token
  validates_presence_of :local_id
  validates_presence_of :title
  validates_presence_of :status
  validates_presence_of :created_on_auto
  validates_uniqueness_of :local_id, :scope => [:user_id, :device_token]

  belongs_to :user
  has_many :effects
  has_many :indicators, through: :effects
  has_many :goals, through: :indicators
  has_many :attachments, dependent: :destroy
  has_many :assets, through: :attachments

  scope :published, -> () { where(status: 'published') }
  scope :submitted, -> () { where(status: 'submitted') }
  scope :submitted_or_published, -> () { where("entries.status = 'submitted' OR entries.status = 'published'") }
  scope :created_since,  -> (date) { where('created_on_auto > ?', date) }
  scope :created_before, -> (date) { where('created_on_auto < ?', date) }
  scope :status, -> (statuses) { where("entries.status = '" + statuses.join("' OR entries.status = '") + "'") }

  def add_attachment user, params, device_token
    asset = user.assets.find_or_create_by(user_id: user.id, key: params['asset_key'], device_token: device_token)
    attachment = self.attachments.find_or_initialize_by(entry_id: self.id, asset_id: asset.id)
    attachment.start_date = DateTime.strptime(params['start_date'].to_s,'%s') if params['start_date']
    attachment.stop_date = DateTime.strptime(params['stop_date'].to_s,'%s') if params['stop_date']
    attachment.save
  end

  def add_attachments user, attachments, device_token
    attachments.each { |attachment| self.add_attachment user, attachment, device_token }
  end

  def add_indicators indicators
    indicators.each do |indicator|
      indicator = indicator.permit(:id)
      indicator = Indicator.where(id: indicator['id']).first
      if indicator && !self.indicators.exists?(indicator.id)
        next if project && project.indicators.exclude?(indicator)
        self.effects.build(indicator_id: indicator.id)
      end
    end
  end

  def project
    self.effects.first && self.effects.first.indicator.project
  end

  def publish
    if self.status == 'submitted'
      self.status = 'published'
      self.save
      UserMailer.entry_published_email(self).deliver
    end
  end

  def archive
    if self.status == 'submitted'
      self.status = 'archived'
      self.save
    end
  end

  def validate_and_save
    if self.status == 'draft'
      self.save
    elsif self.status == 'submitted'
      validate_and_save_submitted
    else
      raise 'Status must be draft or submitted.'
    end
  end

  def validate_and_save_submitted
    if self.project && self.user.field_agent?(self.project)
      self.save
    else
      raise 'Indicator does not exist or cannot be accessed by user'
    end
  end
end
