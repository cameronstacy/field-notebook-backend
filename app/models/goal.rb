class Goal < ApplicationRecord
  validates_presence_of :title
  validates_presence_of :description
  
  belongs_to :project
  has_many :indicators
  has_many :effects, through: :indicators
  has_many :entries, -> { distinct }, through: :effects

  def add_indicators indicators_data
    indicators_data.each do |indicator_data|
      indicator = Indicator.find_or_initialize_by(title: indicator_data['title'])
      unless indicator.goal
        indicator.assign_attributes(indicator_data.select { |key| Indicator.attribute_names.index(key) })
        indicators << indicator
      end
    end
  end
end
