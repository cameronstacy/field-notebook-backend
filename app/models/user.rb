class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable
  # unused devise modules: :confirmable, :omniauthable, :registerable

  has_attached_file :avatar, styles: { thumb: '200x200>' }, default_style: :thumb
  validates_attachment_content_type :avatar, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  validates_attachment_file_name    :avatar, :matches => [/png\Z/, /jpe?g\Z/, /gif\Z/]

  include DeviseTokenAuth::Concerns::User

  has_many :project_users
  has_many :projects, through: :project_users
  has_many :entries
  has_many :assets

  def admin?
    'administrator' == self.permissions || 'impactive' == self.permissions
  end

  def field_agent? project
    roles(project).include? 'field agent'
  end

  def manager? project
    roles(project).include? 'manager'
  end

  def roles project
    project_user = self.project_users.where(project_id: project.id).first
    project_user ? project_user.roles : []
  end

  def send_queued_entries_email
    return if self.entries.submitted.count == 0
    UserMailer.queued_entries_email(self).deliver
  end

  def self.send_all_queued_entries_emails
    User.all.each { |u| u.send_queued_entries_email }
  end
end
