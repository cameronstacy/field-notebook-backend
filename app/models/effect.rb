class Effect < ApplicationRecord
  belongs_to :entry
  has_one :user, through: :entry
  belongs_to :indicator
  has_one :goal, through: :indicator
  has_one :project, through: :goal

  def is_active?
    status.eql?('active')
  end

  def cumulative_value_one
    indicator.cumulative_value_one + (self.value_one || 0)
  end

  def cumulative_value_two
    indicator.cumulative_value_two + (self.value_two || 0)
  end

  def cumulative_value_one=(value)
    self.value_one = value - indicator.cumulative_value_one
  end

  def cumulative_value_two=(value)
    self.value_two = value - indicator.cumulative_value_two
  end
end
