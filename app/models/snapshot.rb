class Snapshot < ApplicationRecord
  belongs_to :project
  belongs_to :user

  def self.create_with_ids(user_id, project_id)
    candidate_id = Snapshot.generate_new_public_id
    until (Snapshot.where(public_id: candidate_id).count == 0) do
        candidate_id = Snapshot.generate_new_public_id
    end
    snapshot = Snapshot.new(user_id: user_id, project_id: project_id, public_id: candidate_id)
    snapshot
  end

  private

  def self.generate_new_public_id
    # Remove I, L, O, U since people misread them occasionally
    charset = %w{ A B C D E F G H J K M N P Q R S T V W X Y Z}
    (0...6).map{ charset.to_a[rand(charset.size)] }.join
  end

end
