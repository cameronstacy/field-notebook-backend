class ProjectUser < ApplicationRecord
  belongs_to :project
  belongs_to :user

  def is_impactive_admin?
    user.admin?
  end

  def is_field_agent?
    roles.include?("field agent")
  end

  def is_project_manager?
    roles.include?("manager")
  end
end
