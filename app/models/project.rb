class Project < ApplicationRecord
  validates_presence_of :title
  validates_presence_of :description

  has_attached_file :hero, styles: { thumb: '200x200>', medium: '400x400>' }, default_style: :thumb
  validates_attachment_content_type :hero, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  validates_attachment_file_name    :hero, :matches => [/png\Z/, /jpe?g\Z/, /gif\Z/]

  has_many :project_users
  has_many :users, through: :project_users
  has_many :goals
  has_many :indicators, through: :goals
  has_many :effects, through: :indicators
  has_many :entries, -> { distinct }, through: :effects

  def add_goals_and_indicators goals_data
    goals_data.each do |goal_data|
      goal = Goal.find_or_initialize_by(title: goal_data['title'])
      unless goal.project
        goal.assign_attributes(goal_data.select { |key| Goal.attribute_names.index(key) })
        goals << goal
      end
      goal.add_indicators goal_data['indicators']
    end
  end

  def add_users users_data
    users_data.each do |user_data|
      user = User.find_or_initialize_by(email: user_data['email'])
      user.assign_attributes(user_data.select { |key| User.attribute_names.index(key) })
      user.password = (0...12).map { (65 + rand(26)).chr }.join
      unless ProjectUser.find_by(project: self, user: user)
        users << user
        project_users.last.roles = user_data['project_roles']
      end
    end
  end

  def managers
    self.project_users.includes(:user).
      select { |project_user| project_user.roles.include? 'manager' }.
      map    { |project_user| project_user.user }
  end

  def roles user
    self.project_users.where(user_id: user.id).first.roles
  end

  def send_queued_entries_email
    return if self.entries.submitted.count == 0
    ProjectMailer.queued_entries_email(self).deliver
  end

  def self.send_all_queued_entries_emails
    Project.all.each { |p| p.send_queued_entries_email }
  end

  def self.create_from_json_file file
    project_data = JSON.parse(file.read)
    project = Project.find_or_initialize_by(title: project_data['title'])
    project.description = project_data['description']
    project.add_users project_data['users']
    project.add_goals_and_indicators project_data['goals']
    project.save!
    project
  end
end
