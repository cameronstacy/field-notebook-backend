require "administrate/base_dashboard"

class EntryDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    effects: Field::HasMany,
    indicators: Field::HasMany,
    goals: Field::HasMany,
    attachments: Field::HasMany,
    assets: Field::HasMany,
    id: Field::Number,
    title: Field::String,
    description: Field::String,
    status: Field::String,
    created_on_manual: Field::DateTime,
    created_on_auto: Field::DateTime,
    latitude_manual: Field::String.with_options(searchable: false),
    latitude_auto: Field::String.with_options(searchable: false),
    longitude_manual: Field::String.with_options(searchable: false),
    longitude_auto: Field::String.with_options(searchable: false),
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    local_id: Field::String,
    featured_image_asset_key: Field::String,
    device_token: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :user,
    :indicators,
    :effects,
    :attachments,
    :device_token,
    :local_id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :title,
    :description,
    :user,
    :goals,
    :indicators,
    :effects,
    :attachments,
    :assets,
    :status,
    :created_on_manual,
    :created_on_auto,
    :created_at,
    :updated_at,
    :local_id,
    :featured_image_asset_key,
    :device_token,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :title,
    :description,
    :indicators,
    :status,
  ].freeze

  # Overwrite this method to customize how entries are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(entry)
  #   "Entry ##{entry.id}"
  # end
end
