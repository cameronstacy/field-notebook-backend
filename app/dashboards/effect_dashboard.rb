require "administrate/base_dashboard"

class EffectDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    entry: Field::BelongsTo,
    user: Field::HasOne,
    indicator: Field::BelongsTo,
    goal: Field::HasOne,
    project: Field::HasOne,
    id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    value_one: Field::Number,
    value_two: Field::Number,
    status: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :user,
    :entry,
    :indicator,
    :value_one,
    :value_two,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :user,
    :entry,
    :indicator,
    :created_at,
    :updated_at,
    :value_one,
    :value_two,
    :status,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user,
    :entry,
    :indicator,
    :value_one,
    :value_two,
    :status,
  ].freeze

  # Overwrite this method to customize how effects are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(effect)
  #   "Effect ##{effect.id}"
  # end
end
