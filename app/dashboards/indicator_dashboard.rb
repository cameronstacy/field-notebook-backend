require "administrate/base_dashboard"

class IndicatorDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    goal: Field::BelongsTo,
    project: Field::HasOne,
    effects: Field::HasMany,
    entries: Field::HasMany,
    id: Field::Number,
    title: Field::String,
    description: Field::String,
    value_type: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    baseline: Field::Number,
    target: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :title,
    :value_type,
    :baseline,
    :target
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :title,
    :description,
    :project,
    :goal,
    :value_type,
    :baseline,
    :target,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :title,
    :description,
    :goal,
    :value_type,
    :baseline,
    :target,
  ].freeze

  # Overwrite this method to customize how indicators are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(indicator)
    "[#{indicator.project.title}] -- #{indicator.id}"
  end
end
