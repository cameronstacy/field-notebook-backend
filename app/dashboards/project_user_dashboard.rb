require "administrate/base_dashboard"

class ProjectUserDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,

    roles: RolesField,

    # built in fields are useful for edit / create UI
    project: Field::BelongsTo.with_options( order: 'title' ),
    user: Field::BelongsTo.with_options( order: 'name' ),

    # custom fields allow underlying data to be sortable (id) but view to be readable (title)
    project_id: ProjectField,
    user_id: UserField,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,

    # Use the custom fields so that sorts work
    :project_id,
    :user_id,

    :roles,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :project,
    :user,
    :id,
    :roles,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :project,
    :user,
    :roles,
  ].freeze

  # Overwrite this method to customize how project users are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(project_user)
  #   "ProjectUser ##{project_user.id}"
  # end
end
