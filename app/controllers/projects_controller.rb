class ProjectsController < ApplicationController
  before_action :authenticate_user!

  def index
    if current_user.admin?
      @projects = Project.all.includes(:entries)
    else
      @projects = current_user.projects.includes(:entries)
    end

    respond_to do |format|
        format.html
    end
  end

  def show
    @project = Project.find(project_params[:id])

    status          = project_params[:status]
    @viewing_pending = (!status.blank? and status.downcase.eql?('pending'))

    @team = @project.project_users.select { |pu| !pu.is_impactive_admin? }

    candidate_entries = @project.entries.published
    candidate_entries = @project.entries.submitted if @viewing_pending

    indicator = indicator_filter(@project)
    @indicator = nil
    if indicator
        candidate_entries = candidate_entries
                                    .joins(:effects)
                                    .where(effects: { indicator_id: indicator.id, status: 'active' })
        @indicator = indicator
    end

    if @viewing_pending
        @entries = candidate_entries.page(params[:page]).per(10).order(created_at: :asc)
    else
        @entries = candidate_entries.page(params[:page]).per(10).order(created_at: :desc)
    end

    @goals      = @project.goals
    @indicators = @project.indicators
    @point_in_time = Time.now

    respond_to do |format|
        format.html
    end
  end

  private

  def indicator_filter(project)
    i_id = project_params[:indicator_id]
    indicator = project.indicators.where(id: i_id).first
    return indicator
  end

  def project_params
    params.permit(:id, :indicator_id, :page, :status)
  end
end
