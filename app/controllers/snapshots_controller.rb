class SnapshotsController < ApplicationController
  layout "report", except: [:create, :share]
  before_action :authenticate_user!, except: [:show]

  def create
    @snapshot = Snapshot.create_with_ids(current_user, snapshot_params[:project_id])
    @snapshot.save!

    @modal_target = snapshot_params[:modal_target]
    @form_target  = snapshot_params[:form_target]
    @url_target   = snapshot_params[:url_target]
  end

  def share
    @snapshot = Snapshot.where(public_id: snapshot_params[:public_id]).first
    @emails = snapshot_params[:email_addresses]

    UserMailer.report_shared_email(current_user, @snapshot, @emails).deliver
  end

  def show
    @snapshot = Snapshot.where(public_id: snapshot_params[:id]).first

    @project = @snapshot.project
    @team    = @project.project_users.select { |pu| !pu.is_impactive_admin? }
    @goals   = @project.goals

    @point_in_time = @snapshot.created_at
    candidate_entries = @project.entries.published.created_before(@point_in_time)

    indicator = indicator_filter(@project)
    @indicator = nil
    if indicator
        candidate_entries = candidate_entries
                                    .joins(:effects)
                                    .where(effects: { indicator_id: indicator.id, status: 'active' })
        @indicator = indicator
    end

    @entries = candidate_entries.page(params[:page]).per(10).order(created_at: :desc)

    respond_to do |format|
        format.html
    end
  end

  private

  def indicator_filter(project)
    i_id = snapshot_params[:indicator_id]
    indicator = project.indicators.where(id: i_id).first
    return indicator
  end


  def snapshot_params
    params.permit([:id, :public_id, :project_id, :modal_target, :form_target, :url_target, :indicator_id, :page, :email_addresses])
  end
end

