class EffectsController < ApplicationController
  before_action :authenticate_user!

  def create
    @target_div   = effect_params[:div_id]

    entry_id      = effect_params[:entry_id]
    indicator_id  = effect_params[:indicator_id]

    entry     = Entry.find(entry_id)
    indicator = Indicator.find(indicator_id)

    @effect = entry.effects.build(indicator: indicator, status: 'active')

    new_effect_value = effect_params[:new_effect_value].to_i || -1
    @scope = "entry[effects][#{new_effect_value}]"

    respond_to do |format|
        format.js { }
    end
  end

  private

  def effect_params
    params.permit([:entry_id, :indicator_id, :div_id, :new_effect_value])
  end
end

