class V20170220::BaseController < ActionController::API
  # This only applies to our controllers, devise_token_auth built-in controllers
  # still (ultimately) derive from ApplicationController. Anything that needs to
  # apply to them should also be replicated in app/controllers/overrides/<controller>,
  # with the corresponding necessary changes to config/routes.rb

  # We don't need protect_from_forgery or skip_before_action :verify_authenticity_token since
  # we're derived from ActionController::API which doesn't have CSRF to begin with

  include DeviseTokenAuth::Concerns::SetUserByToken
  include ActionController::Serialization
end
