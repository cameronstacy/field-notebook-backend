class V20170220::ProjectsController < V20170220::BaseController
	before_action :authenticate_user!

  def create
    if current_user.admin?
      begin
        project = Project.create_from_json_file params['file']
        render json: project, each_serializer: ConsoleProjectSerializer
      rescue => error
        render json: { errors: [error] }, status: 400
      end
    else
      render json: { errors: ['Unauthorized request'] }, status: 401
    end
  end

  def index
    if params['console']
      projects = current_user.admin? ? Project.all.includes(:entries) : current_user.projects.includes(:entries)
      render json: projects, each_serializer: ConsoleProjectSerializer
    else
      render json: current_user.projects.select { |project| current_user.field_agent? project }
    end
  end
end
