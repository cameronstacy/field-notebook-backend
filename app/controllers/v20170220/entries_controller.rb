class V20170220::EntriesController < V20170220::BaseController
  before_action :authenticate_user!

  def create
    response = []
    device_token = request.headers['deviceToken']
    params['entries'].each do |params|
      # Using where instead of find when there's an id sets entry to nil instead of crashing.
      begin
        entry = current_user.entries.where(local_id: params['local_id'], device_token: device_token).first || current_user.entries.new()
        if entry && ['draft', nil].include?(entry.status)
          entry.add_indicators params['indicators'] if params['indicators']
          entry.assign_attributes(entry_params params)
          entry.device_token = device_token
          featured_key = entry.featured_image_asset_key
          if featured_key && !featured_key.blank? && params['attachments'].none? { |a| featured_key == a['asset_key'] }
            raise 'Featured Image is not in attachments.'
          elsif entry.validate_and_save
            entry.add_attachments current_user, params['attachments'], device_token if params['attachments']
          else
            raise 'Unknown error.'
          end
        elsif entry && ['submitted', 'published'].include?(entry.status)
          logger.debug 'Entries cannot be changed once submitted.'
        elsif entry
          logger.debug 'Status must be draft, submitted, or published.'
        else
          logger.debug 'Entry not found.'
        end
      rescue => error
        # Kill these, since they can't be serialized by EntrySerializer
        #entry = { local_id: params['local_id'], device_token: device_token, errors: [error] }
        logger.debug "Raised #{error} for inbound entry #{entry}"
        entry = nil
      end
      response << entry if entry
    end
    render json: response, each_serializer: EntrySerializer
  end

  def index
    index_params = params.permit(:scope, :statuses)
    if 'all' == params['scope']
      entries = current_user.projects.includes(:entries).
          select { |project| current_user.field_agent? project }.
          map { |project| project.entries.status(['published']).created_since(90.days.ago) }.
          flatten
    else
      statuses = params['statuses'] || ['published']
      entries = current_user.entries.status(statuses).created_since(90.days.ago)
    end

    render json: entries
  end

  def show
    entry = current_user.entries.where(id: params['id']).first
    render json: entry
  end

  def update
    entry = Entry.where(id: params['id']).first
    if entry && entry.status == 'submitted' && current_user.manager?(entry.project)
      if params['publish']
        if params['indicators']
          params['indicators'].each do |indicator|
            effect = entry.effects.find_or_initialize_by(indicator_id: indicator['id'])
            effect.value_one = indicator['value_one'] unless indicator['value_one'].nil?
            effect.value_two = indicator['value_two'] unless indicator['value_two'].nil?
            effect.status = indicator['status']       unless indicator['status'].nil?
            effect.save
          end
        end
        entry.publish
      elsif params['archive']
        entry.archive
      end
      render json: entry, serializer: ManagerEntrySerializer
    else
      render json: { errors: ['Unauthorized request'] }, status: 401
    end
  end

  private
    def entry_params params
      params['created_on_auto'] = DateTime.strptime(params['created_on_auto'].to_s,'%s') if params['created_on_auto']
      params['created_on_manual'] = DateTime.strptime(params['created_on_manual'].to_s,'%s') if params['created_on_manual']
      params.permit(:effect_id, :title, :description, :status, :created_on_manual, :created_on_auto, :latitude_manual, :latitude_auto, :longitude_manual, :longitude_auto, :featured_image_asset_key, :local_id)
    end
end
