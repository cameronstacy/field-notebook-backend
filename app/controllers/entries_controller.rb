class EntriesController < ApplicationController
  before_action :authenticate_user!, except: [:show_in_report]

  def show
    @entry = Entry.find(entry_params[:id])

    attachments = @entry.attachments

    @featured_image_attachment = attachments.select { |a|  a.asset.key.eql?(@entry.featured_image_asset_key) }.first
    @other_image_attachments   = attachments.select { |a| !a.asset.key.eql?(@entry.featured_image_asset_key) }

    @goals = @entry.goals
  end

  def show_in_report
    @entry = Entry.find(entry_params[:id])
    @snapshot = Snapshot.where(public_id: report_params[:public_id]).first

    head :unauthorized and return unless ( @entry.project.id.eql?(@snapshot.project_id) and @entry.created_on_auto < @snapshot.created_at )

    attachments = @entry.attachments

    @featured_image_attachment = attachments.select { |a|  a.asset.key.eql?(@entry.featured_image_asset_key) }.first
    @other_image_attachments   = attachments.select { |a| !a.asset.key.eql?(@entry.featured_image_asset_key) }

    @goals = @entry.goals

    render layout: "report"
  end

  def update
    @entry = Entry.find(entry_params[:id])

    input_effects = params[:entry][:effects]
    if !input_effects.nil?
        input_effects.keys.each { |input_id|
            effect = nil

            if input_id.to_i > 0
                effect = @entry.effects.where(id: input_id).first || nil

            else # new entry
                indicator_id = input_effects[input_id][:indicator_id]
                indicator = Indicator.find(indicator_id)

                effect = @entry.effects.build(indicator: indicator) if indicator
            end

            if !effect.nil?
                if input_effects[input_id][:cumulative_value_one]
                    effect.cumulative_value_one = input_effects[input_id][:cumulative_value_one].to_i
                end

                if input_effects[input_id][:cumulative_value_two]
                    effect.cumulative_value_two = input_effects[input_id][:cumulative_value_two].to_i
                end

                if input_effects[input_id][:is_active] and !input_effects[input_id][:is_active].eql?('1')
                    effect.status = 'inactive'
                end

                if effect.changed?
                    effect.save
                end
            end
        }
    end

    if params[:entry][:status] and !params[:entry][:status].blank?
        if params[:entry][:status].eql?('archive')
            @entry.archive
        else
            @entry.publish
        end
    end

    @container = entry_params[:entry][:div_id]

    respond_to do |format|
        format.js { }
    end
  end

  private

  def entry_params
    params.permit([:id, { entry: [ :status, :div_id, effects: [ :cumulative_value_one, :cumulative_value_two, :is_active, :indicator_id ] ] }])
  end

  def report_params
    params.permit([:public_id])
  end
end

