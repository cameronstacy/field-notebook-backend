module Admin
  class ProjectUsersController < Admin::ApplicationController
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = ProjectUser.
    #     page(params[:page]).
    #     per(10)
    # end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   ProjectUser.find_by!(slug: param)
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information


    def transform_roles(model_params)
      role_agent   = model_params.delete(:role_agent)
      role_manager = model_params.delete(:role_manager)

      roles = []
      roles << "field agent" if role_agent
      roles << "manager"     if role_manager
      model_params['roles'] = roles

      return model_params
    end


    def create
      resource = resource_class.new( transform_roles(pu_params) )

      if resource.save
        redirect_to(
          [namespace, resource],
          notice: translate_with_resource("create.success"),
        )
      else
        render :new, locals: {
          page: Administrate::Page::Form.new(dashboard, resource),
        }
      end
    end

    def update
      if requested_resource.update( transform_roles(pu_params) )
        redirect_to(
          [namespace, requested_resource],
          notice: translate_with_resource("update.success"),
        )
      else
        render :edit, locals: {
          page: Administrate::Page::Form.new(dashboard, requested_resource),
        }
      end
    end

    def pu_params
        params.require(:project_user).permit(:role_manager, :role_agent, :project_id, :user_id)
    end

  end
end
