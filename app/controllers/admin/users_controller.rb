module Admin
  class UsersController < Admin::ApplicationController
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = User.
    #     page(params[:page]).
    #     per(10)
    # end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   User.find_by!(slug: param)
    # end

    def create
        resource = User.new( user_params )
        resource.password = resource.password_confirmation = SecureRandom.hex(16)

        if resource.save
          redirect_to(
            [namespace, resource],
            notice: translate_with_resource("create.success"),
          )
        else
          render :new, locals: {
            page: Administrate::Page::Form.new(dashboard, resource),
          }
        end
    end

    def update
        if params[:user][:password].blank?
            params[:user].delete(:password)
            params[:user].delete(:password_confirmation)
        end
        super
    end

    def user_params
        params.require(:user).permit(:name, :image, :email, :permissions, :password, :password_confirmation, :avatar)
    end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information
  end
end
