class ProjectMailer < ApplicationMailer

    def queued_entries_email(project)
        @entries = project.entries.submitted
        @project = project

        mail(to: project.managers.collect { |m| m.email },
             subject: "[#{project.title}] -- Queued entries for #{Date.today}")
    end
end
