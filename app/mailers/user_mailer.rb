class UserMailer < ApplicationMailer

    def queued_entries_email(user)
        @entries = user.entries.submitted

        mail(to: user.email,
             subject: "Submitted entries pending approval on #{Date.today}")
    end

    def entry_published_email(entry)
        @entry = entry

        mail(to: entry.user.email,
             subject: "Entry [#{entry.title}] was approved")
    end

    def report_shared_email(user, snapshot, email_addresses)
        @user = user
        @snapshot = snapshot

        mail(to: email_addresses,
             subject: "Project Report [#{@snapshot.project.title}]")
    end
end
