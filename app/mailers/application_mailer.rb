class ApplicationMailer < ActionMailer::Base
  default from: ENV['MAIL_SENDER'] || 'no-reply@impactivecg.com'
  layout 'mailer'
end
