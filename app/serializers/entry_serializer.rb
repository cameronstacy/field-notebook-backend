class EntrySerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :status, :featured_image_asset_key,
      :latitude_manual, :longitude_manual,
      :created_on_manual, :updated_at, :local_id,
      :user, :attachments, :project, :indicators

  has_one :user

  def attachments
    object.attachments.where(stop_date: nil).map do |attachment|
      AttachmentSerializer.new(attachment, { scope: { current_user: current_user } }).as_json
    end
  end

  def created_on_manual
    object.created_on_manual.to_i if object.created_on_manual
  end

  def indicators
    object.effects.map do |effect|
      {
        id: effect.indicator.id,
        title: effect.indicator.title,
        value_type: effect.indicator.value_type,
        value_one: effect.value_one,
        value_two: effect.value_two
      }
    end
  end

  def project
    if object.project
      {
        id: object.project.id,
        title: object.project.title
      }
    else
      {}
    end
  end

  def updated_at
    object.updated_at.to_i if object.updated_at
  end
end
