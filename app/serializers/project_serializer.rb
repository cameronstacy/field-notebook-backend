class ProjectSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :goals, :users

  def goals
    object.goals.map do |goal|
      indicators = goal.indicators.map do |indicator|
        {
          id: indicator.id,
          title: indicator.title,
          description: indicator.description
        }
      end
      {
        id: goal.id,
        title: goal.title,
        description: goal.description,
        indicators: indicators
      }
    end
  end
end
