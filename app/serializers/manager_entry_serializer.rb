class ManagerEntrySerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :status, :featured_image_asset_key,
      :latitude_auto, :latitude_manual, :longitude_auto, :longitude_manual,
      :created_on_auto, :created_on_manual, :updated_at,
      :user, :attachments, :project, :indicators, :errors

  has_one :user
  has_one :project

  def attachments
    object.attachments.where(stop_date: nil).map do |attachment|
      AttachmentSerializer.new(attachment, { scope: { current_user: scope[:current_user] } }).as_json
    end
  end

  def created_on_auto
    object.created_on_auto.to_i if object.created_on_auto
  end

  def created_on_manual
    object.created_on_manual.to_i if object.created_on_manual
  end

  def indicators
    object.effects.where(status: 'active').map do |effect|
      {
        id: effect.indicator.id,
        title: effect.indicator.title,
        description: effect.indicator.description,
        value_type: effect.indicator.value_type,
        value_one: effect.value_one,
        value_two: effect.value_two,
        value_one_sum: effect.indicator.effects.
                          where(status: 'active').sum(:value_one),
        value_two_sum: effect.indicator.effects.
                          where(status: 'active').sum(:value_two),
        status: effect.status
      }
    end
  end

  def updated_at
    object.updated_at.to_i if object.updated_at
  end
end
