class AttachmentSerializer < ActiveModel::Serializer
  attributes :asset_key, :device_token, :asset_id, :user_id, :start_date, :stop_date, :download_url, :upload_url, :errors

  def asset_key
    object.asset.key
  end

  def asset_id
    object.asset.id
  end

  def device_token
    object.asset.device_token
  end

  def download_url
    s3_path = object.asset.s3_path
    s3_resource = Aws::S3::Resource::new
    s3_object = s3_resource.bucket(ENV['AWS_UPLOAD_BUCKET']).object(s3_path)
    begin
      s3_object.exists?
      s3_object.presigned_url(:get, expires_in: 1.week.to_i)
    rescue
      nil
    end
  end

  def start_date
    object.start_date.to_i if object.start_date
  end

  def stop_date
    object.stop_date.to_i if object.stop_date
  end

  def upload_url
    s3_path = object.asset.s3_path
    s3_resource = Aws::S3::Resource::new
    s3_object = s3_resource.bucket(ENV['AWS_UPLOAD_BUCKET']).object(s3_path)
    begin
      s3_object.exists?
      nil
    rescue
      if object.asset.user == scope[:current_user]
        s3_object.presigned_url(:put, expires_in: 1.week.to_i)
      else
        nil
      end
    end
  end

  def user_id
    object.asset.user_id
  end
end
