class ConsoleProjectSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :goals, :entries, :users

  def entries
    statuses = current_user.manager?(object) ? ['submitted', 'published'] : ['published']
    object.entries.status(statuses).map do |entry|
      ManagerEntrySerializer.new(entry, { scope: { current_user: current_user } }).as_json
    end
  end

  def users
    ActiveModel::Serializer::CollectionSerializer.new(object.users.select { |u| u.permissions != 'impactive' }, each_serializer: UserSerializer)
  end

  def goals
    object.goals.map do |goal|
      indicators = goal.indicators.map do |indicator|
        last_indicator = indicator.effects.joins(:entry).
                          where(status: 'active').
                          where(entries: {status: 'published'}).last
        last_value = last_indicator ? last_indicator.value_one : 0
        {
          id: indicator.id,
          title: indicator.title,
          description: indicator.description,
          type: indicator.value_type,
          baseline: indicator.baseline,
          target: indicator.target,
          value_one_sum: indicator.effects.joins(:entry).
                          where(status: 'active').
                          where(entries: {status: 'published'}).sum(:value_one),
          value_two_sum: indicator.effects.joins(:entry).
                          where(status: 'active').
                          where(entries: {status: 'published'}).sum(:value_two),
          last_value: last_value
        }
      end
      {
        id: goal.id,
        title: goal.title,
        description: goal.description,
        indicators: indicators
      }
    end
  end
end
