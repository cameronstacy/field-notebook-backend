class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :name, :image, :nickname

  def image
    return object.avatar.url if !object.avatar.blank?
    return object.image      if !object.image.blank?
    return nil
  end
end
