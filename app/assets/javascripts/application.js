//= require jquery3
//= require jquery_ujs
//= require popper
//= require bootstrap
//= require clipboard

$(document).ready(function() {
    $('.delete-post-button').each(function() {
        $(this).on("click", function() {
            var form = $(this).data('target');
            $(form).find('#entry_status').val('archive');
            $(form).submit();
        });
    });

    $('.additional-indicator-select').each(function() {
        $(this).on("change", function() {
            var entryId = $(this).data('entryid');
            var target  = $(this).data('target');
            var newEffectValue = $(this).data('neweffectvalue');
            var indicatorId = $(this).val();

            $.get('/effects', {
                  'entry_id':         entryId,
                  'indicator_id':     indicatorId,
                  'div_id':           target,
                  'new_effect_value': newEffectValue
                }, null, 'script');

            // Remove this option and reset drop down
            $(this).find('option[value=' + indicatorId + ']').remove();
            $(this).val('');
            $(this).data('neweffectvalue', newEffectValue - 1);
        });
    });

    var clipboard = new Clipboard('.clipboard-btn');
});
