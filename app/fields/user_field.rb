require "administrate/field/base"

class UserField < Administrate::Field::Base
  def user
    resource.user
  end
end
