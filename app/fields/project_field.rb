require "administrate/field/base"

class ProjectField < Administrate::Field::Base
  def project
    resource.project
  end
end
