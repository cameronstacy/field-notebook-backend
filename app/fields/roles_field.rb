require "administrate/field/base"

class RolesField < Administrate::Field::Base
  def role
    resource
  end

  def role_string
    role_array = []
    role_array << "Agent"   if role.is_field_agent?
    role_array << "Manager" if role.is_project_manager?
    return role_array.join(", ")
  end

  def role_manager
    role.is_project_manager? ? 1 : 0
  end
end
