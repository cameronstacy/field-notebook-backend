source 'https://rubygems.org'
ruby "2.4.0"

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

ruby "2.4.0"

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.1'
gem 'active_model_serializers', '~> 0.10.0'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma', '~> 3.8'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'


gem 'foreman'

# Authentication
gem 'omniauth'
gem 'devise'
gem 'devise_token_auth'

# File storage
gem 'aws-sdk', '~> 2'
gem 'paperclip'

# Versioning
gem 'versionist'

gem 'rack-cors', require: 'rack/cors'

#Email
gem 'postmark-rails', '>= 0.10.0'

#Backing up Heroku PG DB to S3
gem "pgbackups-archive"

gem "newrelic_rpm"

# Admin interface -- latest gem version (0.8.1) doesn't have support for ordering BelongsTo fields
gem "administrate", :git => 'https://github.com/thoughtbot/administrate.git'
# Custom fork until PR is merged: https://github.com/picandocodigo/administrate-field-paperclip/pull/7
gem "administrate-field-paperclip", :git => 'https://github.com/akalra/administrate-field-paperclip.git', :branch => 'empty-attachments'

# User-facing UI
gem "slim-rails"
gem "jquery-rails"
gem 'sass-rails', '~> 5.0'
# 4.0.0.beta still has outstanding bug re: theme primary color override (merge vs full object)
gem "bootstrap", :git => "https://github.com/twbs/bootstrap-rubygem.git"
gem 'uglifier', '>= 1.3.0'
gem 'turbolinks', '~> 5'
gem 'kaminari'
gem 'clipboard-rails'


group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri

  gem 'pry'
  gem 'pry-rails'

  # Use RSpec for specs
  gem 'rspec-rails', '>= 3.5.0'

  # Use Factory Girl for generating random test data
  gem 'factory_girl_rails'
end

group :development do
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
	gem 'simplecov', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
