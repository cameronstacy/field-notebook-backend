require 'rails_helper'

RSpec.describe V20170220::EntriesController, type: :controller do

  PROJECT_ONE_TITLE = 'Project One Title'
  PROJECT_TWO_TITLE = 'Project Two Title'
  PROJECT_THREE_TITLE = 'Project Three Title'
  GOAL_ONE_TITLE = 'Goal One Title'
  GOAL_TWO_TITLE = 'Goal Two Title'
  GOAL_THREE_TITLE = 'Goal Three Title'
  INDICATOR_ONE_TITLE = 'Indicator One Title'
  INDICATOR_TWO_TITLE = 'Indicator Two Title'
  INDICATOR_THREE_TITLE = 'Indicator Three Title'
  ENTRY_1_TITLE = 'Entry One Title'
  ENTRY_2_TITLE = 'Entry Two Title'
  ENTRY_3_TITLE = 'Entry Three Title'
  ENTRY_4_TITLE = 'Entry Four Title'
  ENTRY_5_TITLE = 'Entry Five Title'
  ENTRY_6_TITLE = 'Entry Six Title'
  ENTRY_7_TITLE = 'Entry Seven Title'
  ENTRY_8_TITLE = 'Entry Eight Title'
  ENTRY_9_TITLE = 'Entry Nine Title'
  ENTRY_10_TITLE = 'Entry Ten Title'
  ENTRY_11_TITLE = 'Entry Eleven Title'
  ENTRY_12_TITLE = 'Entry Twelve Title'
  ALL_PUBLISHED_ENTRY_TITLES = [ENTRY_1_TITLE, ENTRY_2_TITLE, ENTRY_5_TITLE]
  USER_PUBLISHED_ENTRY_TITLES = [ENTRY_1_TITLE, ENTRY_3_TITLE, ENTRY_5_TITLE]
  VALID_MANAGER_ENTRY_TITLES = [ENTRY_5_TITLE, ENTRY_6_TITLE, ENTRY_8_TITLE, ENTRY_10_TITLE]

  user = nil
  entry = nil
  indicator = nil

  describe 'POST #create when not logged in' do
    it 'returns http unauthorized' do
      post :create

      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe 'POST #create when logged in' do
    after(:each) do
      user = nil
      entry = nil
    end

    before(:each) do
      user = FactoryGirl.create :user
      user.save
      request.headers.merge!(user.create_new_auth_token)
      request.headers.merge!({ 'deviceToken': 'someToken' })
    end

    before(:all) do
        ENV['AWS_UPLOAD_BUCKET'] = 'us-east-2'
        Aws.config[:stub_responses] = true
    end

    it 'returns http success when passed valid params' do
      post :create, params: {entries: [draft_entry]}

      expect(response).to have_http_status(:success)
    end

    it 'returns an error when passed invalid params' do
      entry = draft_entry
      entry.delete :title
      post :create, params: {entries: [entry]}

      expect(JSON.parse(response.body)).to eq([])
    end

    it 'returns http success when status is submitted and params are valid' do
      project = create :project
      create :project_user, project: project, user: user
      goal = create :goal, project: project
      indicator = create :indicator, goal: goal

      entry = draft_entry
      entry[:status] = 'submitted'
      entry[:indicators] = [{ id: indicator.id }]
      post :create, params: { entries: [entry] }

      expect(response).to have_http_status(:success)
    end

    it 'returns an error when status is submitted and effect_id is not present' do
      entry = draft_entry
      entry[:status] = 'submitted'
      post :create, params: {entries: [entry]}

      expect(JSON.parse(response.body)).to eq([])
    end

    it 'returns an error when status is submitted and goal_id is not accessible to user' do
      project = create :project
      goal = create :goal, project: project

      entry = draft_entry
      entry[:status] = 'submitted'
      entry[:goal_id] = goal.id
      post :create, params: {entries: [entry]}

      expect(JSON.parse(response.body)).to eq([])
    end

    it 'returns an error when status is neither draft nor submitted' do
      entry = draft_entry
      entry[:status] = 'published'
      post :create, params: {entries: [entry]}

      expect(JSON.parse(response.body)).to eq([])
    end

    it 'returns an array of one object with a url when passed an attachment' do
      attachment = {
        asset_key: 'foobar'
      }
      entry = draft_entry
      entry[:attachments] = [attachment]
      post :create, params: {entries: [entry] }

      expect(JSON.parse(response.body)[0]['attachments'][0]).to have_key('asset_key')
    end

    it 'ignore changes to already submitted entries' do
      entry = FactoryGirl.create :entry, user: user, title: ENTRY_1_TITLE, status: 'submitted'
      entry_params = draft_entry
      entry_params[:local_id] = entry.local_id
      post :create, params: { entries: [entry_params] }

      expect(entry.title).to eq(entry.title)
      expect(entry.status).to eq('submitted')
    end
  end

  describe 'GET #index when not logged in' do
    it 'returns http unauthorized' do
      get :index

      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe 'GET #index when logged in' do
    after(:each) do
      user = nil
      entry = nil
    end

    before(:each) do
      user = FactoryGirl.create :user
      user2 = FactoryGirl.create :user, email: 'second_test_email@example.com'
      project1 = FactoryGirl.create :project, title: PROJECT_ONE_TITLE
      project2 = FactoryGirl.create :project, title: PROJECT_TWO_TITLE
      project3 = FactoryGirl.create :project, title: PROJECT_THREE_TITLE
      goal1 = FactoryGirl.create :goal, project: project1, title: GOAL_ONE_TITLE
      goal2 = FactoryGirl.create :goal, project: project2, title: GOAL_TWO_TITLE
      goal3 = FactoryGirl.create :goal, project: project3, title: GOAL_THREE_TITLE
      indicator1 = FactoryGirl.create :indicator, goal: goal1, title: INDICATOR_ONE_TITLE
      indicator2 = FactoryGirl.create :indicator, goal: goal2, title: INDICATOR_TWO_TITLE
      indicator3 = FactoryGirl.create :indicator, goal: goal3, title: INDICATOR_THREE_TITLE
      FactoryGirl.create :project_user, project: project1, user: user
      FactoryGirl.create :project_user, project: project3, user: user, roles: ['manager']

      entry1 = FactoryGirl.create :entry, user: user, title: ENTRY_1_TITLE, status: 'published', local_id: 'a'
      FactoryGirl.create :effect, entry: entry1, indicator: indicator1
      entry2 = FactoryGirl.create :entry, user: user2, title: ENTRY_2_TITLE, status: 'published', local_id: 'b'
      FactoryGirl.create :effect, entry: entry2, indicator: indicator1
      entry3 = FactoryGirl.create :entry, user: user, title: ENTRY_3_TITLE, status: 'published', local_id: 'c'
      FactoryGirl.create :effect, entry: entry3, indicator: indicator2
      entry4 = FactoryGirl.create :entry, user: user2, title: ENTRY_4_TITLE, status: 'published', local_id: 'd'
      FactoryGirl.create :effect, entry: entry4, indicator: indicator2
      entry5 = FactoryGirl.create :entry, user: user, title: ENTRY_5_TITLE, status: 'published', local_id: 'e'
      FactoryGirl.create :effect, entry: entry5, indicator: indicator3
      entry6 = FactoryGirl.create :entry, user: user2, title: ENTRY_6_TITLE, status: 'submitted', local_id: 'f'
      FactoryGirl.create :effect, entry: entry6, indicator: indicator3
      entry7 = FactoryGirl.create :entry, user: user, title: ENTRY_7_TITLE, created_on_manual: 91.days.ago, local_id: 'g'
      FactoryGirl.create :effect, entry: entry7, indicator: indicator1
      entry8 = FactoryGirl.create :entry, user: user2, title: ENTRY_8_TITLE, created_on_manual: 91.days.ago, local_id: 'h'
      FactoryGirl.create :effect, entry: entry8, indicator: indicator3
      entry9 = FactoryGirl.create :entry, user: user, title: ENTRY_9_TITLE, status: 'draft', local_id: 'i'
      FactoryGirl.create :effect, entry: entry9, indicator: indicator1
      entry10 = FactoryGirl.create :entry, user: user2, title: ENTRY_10_TITLE, status: 'submitted', local_id: 'j'
      FactoryGirl.create :effect, entry: entry10, indicator: indicator3

      request.headers.merge!(user.create_new_auth_token)
    end

    it 'returns http success' do
      get :index

      expect(response).to have_http_status(:success)
    end

    it "should return user's published entries when passed no params" do
      get :index
      body = JSON.parse response.body

      body.each do |entry|
        expect(USER_PUBLISHED_ENTRY_TITLES.include? entry['title']).to be true
      end
    end

    it "should return all published entries for user's projects when passed scope=all param" do
      get :index, scope: 'all'
      body = JSON.parse response.body

      body.each do |entry|
        expect(ALL_PUBLISHED_ENTRY_TITLES.include? entry['title']).to be true
      end
    end
  end

  describe 'GET #show when not logged in' do
    it 'returns http unauthorized' do
      get :show, params: { id: 1 }

      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe 'GET #show when logged in' do
    after(:each) do
      user = nil
      entry = nil
    end

    before(:each) do
      user = FactoryGirl.create :user

      entry = FactoryGirl.create :entry, user: user, title: ENTRY_1_TITLE, status: 'draft'

      request.headers.merge!(user.create_new_auth_token)
    end

    it 'returns http success' do
      get :show, params: { id: entry.id }

      expect(response).to have_http_status(:success)
    end

    it "should return the entry if it belongs to user" do
      get :show, params: { id: entry.id }
      body = JSON.parse response.body

      expect(body['title']).equal? ENTRY_1_TITLE
    end
  end

  describe 'post #update when not logged in' do
    it 'returns http unauthorized' do
      user = FactoryGirl.create :user
      entry = FactoryGirl.create :entry, user: user, title: ENTRY_1_TITLE, status: 'draft'

      post :create, params: { id: entry.id }

      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe 'post #update when logged in' do
    after(:each) do
      user = nil
      entry = nil
      indicator = nil
    end

    before(:each) do
      user = FactoryGirl.create :user
      project = FactoryGirl.create :project, title: PROJECT_ONE_TITLE
      FactoryGirl.create :project_user, user: user, project: project, roles: ['manager']
      goal = FactoryGirl.create :goal, project: project, title: GOAL_ONE_TITLE
      indicator = FactoryGirl.create :indicator, goal: goal, title: INDICATOR_ONE_TITLE
      entry = FactoryGirl.create :entry, user: user, title: ENTRY_1_TITLE, status: 'submitted'
      FactoryGirl.create :effect, entry: entry, indicator: indicator

      request.headers.merge!(user.create_new_auth_token)
    end

    it 'returns http success with valid parameters' do
      put :update, params: { id: entry.id, entry: draft_entry }

      expect(response).to have_http_status(:success)
    end

    it 'returns http unauthorized when status is draft' do
      entry.update(status: 'draft')
      put :update, params: { id: entry.id, entry: draft_entry }

      expect(response).to have_http_status(:unauthorized)
    end

    it 'returns http unauthorized when status is published' do
      entry.update(status: 'published')
      put :update, params: { id: entry.id, entry: draft_entry }

      expect(response).to have_http_status(:unauthorized)
    end

    it 'returns a published entry when passed a "publish" param' do
      put :update, params: { id: entry.id, entry: draft_entry, publish: true }

      body = JSON.parse response.body
      expect(body['status']).to eq('published')
    end

    it 'returns an entry with updated indictor value when passed an indicator with a new value' do
      put :update, params: { id: entry.id, publish: 'true', indicators: [{id: indicator.id, value_one: 42}] }

      body = JSON.parse response.body
      expect(body['indicators'][0]['value_one']).to eq(42)
    end
  end

end

def draft_entry
  {
    title: 'Test Entry',
    description: 'The description for the test entry.',
    status: 'draft',
    created_on_manual: 20.days.ago.to_i,
    created_on_auto: 50.days.ago.to_i,
    latitude_manual: 38.8977,
    latitude_auto: 55.7520,
    longitude_manual: 77.0365,
    longitude_auto: 37.6175,
    local_id: rand(1..9999999).to_s
  }
end
