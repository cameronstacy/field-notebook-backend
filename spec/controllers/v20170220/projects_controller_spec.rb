require 'rails_helper'

RSpec.describe V20170220::ProjectsController, type: :controller do

  user = nil
  project = nil

  describe 'GET #index when not logged in' do
    it 'returns http unauthorized' do
      get :index
      expect(response).to have_http_status(:unauthorized)
    end
  end

  describe 'GET #index when logged in' do
    after(:each) do
      user = nil
      project = nil
    end

  	before(:each) do
  		user = FactoryGirl.create :user
  		project = FactoryGirl.create :project
      FactoryGirl.create :project_user, project: project, user: user
  		FactoryGirl.create :goal, project: project
    	request.headers.merge!(user.create_new_auth_token)

      get :index
  	end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it "returns a list of the user's projects and goals" do
    	body = JSON.parse response.body

    	user.projects.each_with_index do |p, i|
    		expect(body[i]['id']).to eq(p.id)
    		expect(body[i]['title']).to eq(p.title)
    		expect(body[i]['description']).to eq(p.description)
    		p.goals.each_with_index do |g, j|
	    		expect(body[i]['goals'][j]['id']).to eq(p.goals[j].id)
	    		expect(body[i]['goals'][j]['title']).to eq(p.goals[j].title)
	    		expect(body[i]['goals'][j]['description']).to eq(p.goals[j].description)
	    	end
    	end
    end
  end

end
