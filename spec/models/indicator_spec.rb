require 'rails_helper'

RSpec.describe Indicator, type: :model do
  it 'is valid with valid attributes' do
    project = create :project
    goal = create :goal, project: project
    indicator = create :indicator, goal: goal

    expect(indicator).to be_valid
  end

  it "is not valid without a goal" do
    indicator = build :indicator

    expect(indicator).to_not be_valid
  end

  it "is not valid without a title" do
    project = create :project
    goal = create :goal, project: project
    indicator = build :indicator, goal: goal, title: nil

    expect(indicator).to_not be_valid
  end

  it "is not valid without a description" do
    project = create :project
    goal = create :goal, project: project
    indicator = build :indicator, goal: goal, description: nil
    
    expect(indicator).to_not be_valid
  end
end
