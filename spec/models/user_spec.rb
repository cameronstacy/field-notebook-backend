require 'rails_helper'

RSpec.describe User, type: :model do
  it 'is valid with valid attributes' do
    user = build :user
  	expect(user).to be_valid
  end

  describe 'roles' do
    [0, 1, 2].each do |i|
      it 'has the correct role for index ' + i.to_s do
        project = create :project
        user = create :user
        project_user = create_project_user project, user
        expected = project_user.roles[i]

        expect(user.roles(project)[i]).to eq(expected)
      end
    end
  end
end
