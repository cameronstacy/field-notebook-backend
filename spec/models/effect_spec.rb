require 'rails_helper'

RSpec.describe Effect, type: :model do
  it 'is valid with valid attributes' do
    project = create :project
    user = create :user
    project_user = create_project_user project, user
    goal = create :goal, project: project
    indicator = create :indicator, goal: goal
    effect = create :effect, indicator: indicator, user: user

    expect(effect).to be_valid
  end
end
