require 'rails_helper'

RSpec.describe ProjectUser, type: :model do
  it 'is valid with valid attributes' do
    project = create :project
    user = create :user
  	project_user = create_project_user project, user
  	expect(project_user).to be_valid
  end
end
