require 'rails_helper'

RSpec.describe Asset, type: :model do
  it 'is valid with valid attributes' do
    user = create :user
    asset = build :asset, user: user
    expect(asset).to be_valid
  end
end
