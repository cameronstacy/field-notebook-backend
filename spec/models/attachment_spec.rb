require 'rails_helper'

RSpec.describe Attachment, type: :model do
  it 'is valid with valid attributes' do
    user = create :user
    entry = create :entry, user: user
    asset = create :asset, user: user
    attachment = create :attachment, asset: asset, entry: entry

    expect(attachment).to be_valid
  end
end
