require 'rails_helper'


RSpec.describe Project, type: :model do
  it 'is valid with valid attributes' do
  	project = build(:project)
  	expect(project).to be_valid
  end

  it "is not valid without a title" do
    project = build(:project, title: nil)
    expect(project).to_not be_valid
  end

  it "is not valid without a description" do
    project = build(:project, description: nil)
    expect(project).to_not be_valid
  end

  describe 'roles' do
    [0, 1, 2].each do |i|
      it 'has the correct role for index ' + i.to_s do
        project = create :project
        user = create :user
        project_user = create_project_user project, user
        expected = project_user.roles[i]

        expect(project.roles(user)[i]).to eq(expected)
      end
    end
  end
end
