require 'rails_helper'

RSpec.describe Entry, type: :model do
  it 'is valid with valid attributes' do
  	user = create :user
  	entry = build :entry, user_id: user.id
  	expect(entry).to be_valid
  end

  it "is not valid without a title" do
  	user = create :user
    entry = build :entry, user_id: user.id, title: nil
    expect(entry).to_not be_valid
  end

  it "is not valid without a status" do
  	user = create :user
    entry = build :entry, user_id: user.id, status: nil
    expect(entry).to_not be_valid
  end

  it "is not valid without a created_on_auto" do
  	user = create :user
    entry = build :entry, user_id: user.id, created_on_auto: nil
    expect(entry).to_not be_valid
  end
end
