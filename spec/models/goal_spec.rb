require 'rails_helper'

RSpec.describe Goal, type: :model do
  it 'is valid with valid attributes' do
  	project = create :project
  	goal = build :goal, project_id: project.id

  	expect(goal).to be_valid
  end

  it "is not valid without a title" do
    project = create :project
    goal = build :goal, project_id: project.id, title: nil

    expect(goal).to_not be_valid
  end

  it "is not valid without a description" do
    project = create :project
    goal = build :goal, project_id: project.id, description: nil
    
    expect(goal).to_not be_valid
  end
end
