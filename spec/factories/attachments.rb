FactoryGirl.define do
  factory :attachment do
    asset nil
    entry nil
    start_date "2017-02-09 12:28:34"
    stop_date "2017-02-09 12:28:34"
  end
end
