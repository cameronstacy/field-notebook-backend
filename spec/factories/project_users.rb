FactoryGirl.define do
  factory :project_user do
    project nil
    user nil
    roles ['field agent']
  end
end
