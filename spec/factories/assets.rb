FactoryGirl.define do
  factory :asset do
    user nil
    key 'mushroom.jpg'
    device_token 'some_other_token'
  end
end
