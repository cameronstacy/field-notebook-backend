FactoryGirl.define do
  factory :indicator do
    goal nil
    title "MyString"
    description "MyString"
    value_type "MyString"
  end
end
