FactoryGirl.define do
  factory :goal do
    title 'Test goal'
    description 'This is the description for the test goal.'
    project nil
  end
end
