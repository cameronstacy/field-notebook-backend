FactoryGirl.define do
  factory :effect do
    indicator nil
    entry nil
    value_one 1
    value_two 2
    status 'active'
  end
end
