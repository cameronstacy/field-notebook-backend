FactoryGirl.define do
  factory :entry do
    effects []
    user nil
    title 'Test entry'
    description 'Test entry description'
    status 'draft'
    created_on_manual 30.days.ago
    created_on_auto 50.days.ago
    latitude_manual 38.8977
    latitude_auto 55.7520
    longitude_manual 77.0365
    longitude_auto 37.6175
    local_id 'an id from the device'
    device_token 'some_token'
  end
end
