FactoryGirl.define do
  factory :project do
    title "Test Project"
    description "This is the description for the test project."
  end
end
